﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Shoot : MonoBehaviour
{
    AudioSource audioData;

    public float NumberOfBullets = 30;

    private bool shooting = false;
    private bool canShoot = true;

    void Start()
    {
        audioData = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if(canShoot)
        {
            if (Input.GetMouseButton(0))
            {
                shooting = true;
            }

            if (shooting)
            {
                audioData.Play(0);
                NumberOfBullets = (NumberOfBullets - 1);
                Debug.Log("shot");
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            audioData.Pause();
            shooting = false;
        }

        if (NumberOfBullets < 1)
        {
            audioData.Pause();
            canShoot = false;
            shooting = false;
            NumberOfBullets = 30;
        }
        if (NumberOfBullets > 1)
        {
            canShoot = true;
        }
    }
}
